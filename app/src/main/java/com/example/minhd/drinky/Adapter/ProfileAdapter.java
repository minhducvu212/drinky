package com.example.minhd.drinky.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.minhd.drinky.Common.ItemProfile;
import com.example.minhd.drinky.R;

/**
 * Created by minhd on 17/11/23.
 */

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.CommonHolder> {

    private ICom mInter;
    private Context mContext ;

    public ProfileAdapter(Context context , ICom iCom) {
        mInter = iCom;
        mContext = context ;
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    @Override
    public CommonHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_profile, parent, false);
        return new CommonHolder(view);
    }

    @Override
    public void onBindViewHolder(CommonHolder commonHolder, int i) {
        ItemProfile item = mInter.getItem(i) ;
        if (i % 2 == 0) {
            commonHolder.img.setImageResource(R.drawable.item1);
        } else {
            commonHolder.img.setImageResource(R.drawable.item2);
        }
    }


    @Override
    public int getItemCount() {
        return mInter.getCount();
    }

    static class CommonHolder extends RecyclerView.ViewHolder {

        private ImageView img;

        public CommonHolder(View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img_tra) ;
        }
    }

    public interface ICom {
        int getCount();

        ItemProfile getItem(int position);
    }

    public Bitmap roundCornerImage(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

}

