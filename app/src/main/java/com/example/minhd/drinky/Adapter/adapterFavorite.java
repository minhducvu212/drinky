package com.example.minhd.drinky.Adapter;

import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.minhd.drinky.Common.Item;
import com.example.minhd.drinky.Fragment.MapFragment;
import com.example.minhd.drinky.R;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static com.example.minhd.drinky.Activity.MainActivity.geocoderr;
import static com.example.minhd.drinky.Activity.MainActivity.latLng;

/**
 * Created by minhd on 17/11/21.
 */

public class adapterFavorite  extends RecyclerView.Adapter<adapterFavorite.CommonHolder>{

    private ICom mInter;
    private Context mContext ;

    public adapterFavorite(ICom iCom, Context context){
        mInter = iCom ;
        mContext = context ;
    }
    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    @Override
    public CommonHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item, parent, false);
        return new CommonHolder(view) ;
    }

    @Override
    public void onBindViewHolder(CommonHolder holder, int position) {
        Item item = mInter.getItem(position) ;
//        holder.tvName.setText(item.getName());
//        holder.tvLoc.setText(item.getLoc());
        if (position % 4 == 1) {
            holder.img.setImageResource(R.drawable.item0_fav);
        }else if (position % 4 == 3) {
            holder.img.setImageResource(R.drawable.item2_fav);
        }else if (position % 4 == 4) {
            holder.img.setImageResource(R.drawable.item3_fav);
        }

        holder.imgCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:0123456789"));
                mContext.startActivity(intent);
            }
        });

        holder.imgNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, MapFragment.class);
                Bundle bundle = new Bundle();
                String origin = getLocation(latLng);
                String destination = "112 Trần Duy Hưng";
                bundle.putString("origin", origin);
                bundle.putString("destination", destination);
                intent.putExtra("Bundel", bundle);
                mContext.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return mInter.getCount();
    }

    static class CommonHolder extends RecyclerView.ViewHolder{

        private TextView tvName, tvLoc ;
        private ImageView img, imgCall, imgNav;

        public CommonHolder(View itemView) {
            super(itemView);
//            tvName = itemView.findViewById(R.id.name) ;
//            tvLoc = itemView.findViewById(R.id.loc) ;
            img = itemView.findViewById(R.id.iv_mainnn);


             imgCall = itemView.findViewById(R.id.img_fav_call);
             imgNav = itemView.findViewById(R.id.img_fav_nav);


        }
    }

    public interface ICom {
        int getCount();
        Item getItem(int position) ;
    }

    private String getLocation(LatLng latLngg) {
        try {
            geocoderr = new Geocoder(mContext, Locale.getDefault());
            List<Address> addresses =
                    geocoderr.getFromLocation(latLngg.latitude, latLngg.longitude, 1);
            if (addresses == null || addresses.size() == 0) {
                return null;
            }
            String result = "";
            int maxLine = addresses.get(0).getMaxAddressLineIndex();
            result = addresses.get(0).getAddressLine(0);
            for (int i = 1; i < maxLine; i++) {
                result += ", " + addresses.get(0).getAddressLine(i);
            }
            result += ", " + addresses.get(0).getCountryName();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


}
