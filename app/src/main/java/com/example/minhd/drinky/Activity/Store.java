package com.example.minhd.drinky.Activity;

import android.app.Dialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.minhd.drinky.Common.Pager;
import com.example.minhd.drinky.Fragment.MapFragment;
import com.example.minhd.drinky.R;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import static com.example.minhd.drinky.Activity.MainActivity.geocoderr;
import static com.example.minhd.drinky.Activity.MainActivity.latLng;

public class Store extends AppCompatActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private int[] images = {R.drawable.dingtea_show, R.drawable.dingteaa, R.drawable.foody,
            R.drawable.dingtea_show, R.drawable.dingteaa, R.drawable.foody,R.drawable.dingtea_show, R.drawable.dingteaa, R.drawable.foody,
            R.drawable.dingtea_show, R.drawable.dingteaa, R.drawable.foody,R.drawable.dingtea_show, R.drawable.dingteaa, R.drawable.foody};

    private int[] images2 = {R.drawable.toco, R.drawable.dingtea, R.drawable.feelingtea, R.drawable.hk,
            R.drawable.royaltea, R.drawable.saytea, R.drawable.boba, R.drawable.uncletea,
            R.drawable.toco, R.drawable.dingtea, R.drawable.feelingtea, R.drawable.hk,
            R.drawable.royaltea, R.drawable.saytea, R.drawable.boba, R.drawable.uncletea};

    private View cell;
    private LinearLayout mainLayout, mainLayout2;
    private ViewPager viewPager, viewPager2;
    private ImageView imgBack, imgSearch, imgBinhLuan;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarTranslucent(true);
        setContentView(R.layout.activity_store);

        mToolbar = findViewById(R.id.toolbar_store);
        imgSearch = findViewById(R.id.img_searchStore);

        imgBinhLuan = findViewById(R.id.img_binh_luan);
        imgBinhLuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(Store.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.rate);
                Window window = dialog.getWindow();
                window.setLayout(1000, 1000);
                dialog.show();

//
                dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.findViewById(R.id.btn_submit).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(Store.this, "Submit done...", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });

            }
        });


        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), SearchActivity.class);
                startActivity(intent);
            }
        });
        setSupportActionBar(mToolbar);
        try {
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        } catch (NullPointerException e) {
        }

        mToolbar.setTitle("");
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        imgBack = findViewById(R.id.img_back);
        imgSearch = findViewById(R.id.img_searchStore);

        imgBack.setOnClickListener(this);
        imgSearch.setOnClickListener(this);

        viewPager = findViewById(R.id._viewPagerStore);

        mainLayout = findViewById(R.id.linearLayoutStore);

        for (int i = 0; i < images.length; i++) {

            cell = getLayoutInflater().inflate(R.layout.cell_store, null);

            final ImageView imageView = cell.findViewById(R.id._image);

            imageView.setOnClickListener(v -> {

                viewPager.setVisibility(View.VISIBLE);
                viewPager.setAdapter
                        (new Pager(Store.this, images));
                viewPager.setCurrentItem(v.getId());
            });

            imageView.setId(i);


            imageView.setImageResource(images[i]);

            mainLayout.addView(cell);
        }

        viewPager2 = findViewById(R.id._viewPagerStore2);

        mainLayout2 = findViewById(R.id.linearLayoutStore2);

        for (int i = 0; i < images2.length; i++) {

            cell = getLayoutInflater().inflate(R.layout.cell_store_2, null);

            final ImageView imageView = cell.findViewById(R.id._image);

            imageView.setOnClickListener(v -> {

                viewPager2.setVisibility(View.VISIBLE);
                viewPager2.setAdapter
                        (new Pager(Store.this, images2));
                viewPager2.setCurrentItem(v.getId());
            });

            imageView.setId(i);


            imageView.setImageResource(images2[i]);

            mainLayout2.addView(cell);
        }
        ImageView imgCall =findViewById(R.id.ic_call_store);
        ImageView imgNav = findViewById(R.id.ic_nav_store);

        imgCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:0123456789"));
                startActivity(intent);
            }
        });

        imgNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getBaseContext(), MapFragment.class);
                Bundle bundle = new Bundle();
                String origin = getLocation(latLng);
                String destination = "112 Trần Duy Hưng";
                bundle.putString("origin", origin);
                bundle.putString("destination", destination);
                intent.putExtra("Bundel", bundle);
                startActivity(intent);
            }
        });


    }

    private String getLocation(LatLng latLngg) {
        try {
            geocoderr = new Geocoder(getBaseContext(), Locale.getDefault());
            List<Address> addresses =
                    geocoderr.getFromLocation(latLngg.latitude, latLngg.longitude, 1);
            if (addresses == null || addresses.size() == 0) {
                return null;
            }
            String result = "";
            int maxLine = addresses.get(0).getMaxAddressLineIndex();
            result = addresses.get(0).getAddressLine(0);
            for (int i = 1; i < maxLine; i++) {
                result += ", " + addresses.get(0).getAddressLine(i);
            }
            result += ", " + addresses.get(0).getCountryName();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_back:
                onBackPressed();
                break;

            case R.id.img_searchStore:
                Intent intent1 = new Intent(getBaseContext(), SearchActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent1);
                finish();
                break;

            default:

        }
    }

    @Override
    public void onBackPressed() {

        Bundle bundle = null;
        String value = null;

        try {
            bundle = getIntent().getExtras();
            value = bundle.getString("Value");

        } catch (NullPointerException e) {
        }

        if (bundle != null && value.equals("Main")) {
            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();

        } else {
            Intent intent = new Intent(getBaseContext(), MapFragment.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP |Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            finish();

        }
    }
}
