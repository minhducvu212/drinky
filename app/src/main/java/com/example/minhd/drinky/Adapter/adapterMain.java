package com.example.minhd.drinky.Adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.minhd.drinky.Activity.Store;
import com.example.minhd.drinky.Common.ItemMain;
import com.example.minhd.drinky.LongPress.LongPressActivity;
import com.example.minhd.drinky.R;

/**
 * Created by minhd on 17/11/29.
 */

public class adapterMain extends RecyclerView.Adapter<adapterMain.CommonHolder>{

    private adapterMain.ICom mInter;
    private Context mContext ;

    private int[] images = {R.drawable.toco, R.drawable.dingtea, R.drawable.toco, R.drawable.dingtea,
            R.drawable.toco, R.drawable.dingtea, R.drawable.toco, R.drawable.dingtea,
            R.drawable.toco, R.drawable.dingtea, R.drawable.toco, R.drawable.dingtea,
            R.drawable.toco, R.drawable.dingtea, R.drawable.toco, R.drawable.dingtea};

    private String[] namess = {"Toco Toco", "Ding Tea", "Feeling Tea", "Gongcha", "KFC", "Lotte", "HighLands", "Uncle Tea",
            "Toco Toco", "Ding Tea", "Feeling Tea", "Gongcha", "KFC", "Lotte", "HighLands", "Uncle Tea"};

    private String[] locss = {"59 Hồ Tùng Mậu", "111 trần Duy Hưng", "122 Xuân Thủy", "36 Trần Quốc Hoàn",
            "69 Phạm Văn Đồng", "36 Phạm Hùng", "43 Phạm Văn Đồng", "96 Cấu Giấy",
            "59 Hồ Tùng Mậu", "111 trần Duy Hưng", "122 Xuân Thủy", "36 Trần Quốc Hoàn",
            "69 Phạm Văn Đồng", "36 Phạm Hùng", "43 Phạm Văn Đồng", "96 Cấu Giấy"};

    private int[] imagesShow = {R.drawable.dingteaa, R.drawable.foody, R.drawable.trasua_feeling, R.drawable.dingtea_show,
            R.drawable.dingteaa, R.drawable.foody, R.drawable.trasua_feeling, R.drawable.dingtea_show,
            R.drawable.dingteaa, R.drawable.foody, R.drawable.trasua_feeling, R.drawable.dingtea_show,
            R.drawable.dingteaa, R.drawable.foody, R.drawable.trasua_feeling, R.drawable.dingtea_show};


    public adapterMain(adapterMain.ICom iCom , Context context){
        mInter = iCom ;
        mContext = context ;
    }
    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    @Override
    public adapterMain.CommonHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.cell, parent, false);
        return new adapterMain.CommonHolder(view) ;
    }

    @Override
    public void onBindViewHolder(adapterMain.CommonHolder holder, int position) {
        ItemMain item = mInter.getItem(position) ;

        holder.img.setImageResource(item.getImg());

        String name = namess[position];
        String loc = locss[position];
        int imgg = images[position];
        int imggAva = imagesShow[position] ;
        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, Store.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                Bundle bundle = new Bundle();
                bundle.putString("Value", "Main");
                intent.putExtra("Bundle",bundle);
                intent.putExtras(bundle) ;
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                mContext.startActivity(intent);
            }
        });

        holder.img.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

//                showDetails(imgg,imggAva, name, loc);

                Intent intent = new Intent(mContext, LongPressActivity.class) ;
                mContext.startActivity(intent);

                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return mInter.getCount();
    }

    static class CommonHolder extends RecyclerView.ViewHolder{

        private ImageView img;

        public CommonHolder(View itemView) {
            super(itemView);

            img = itemView.findViewById(R.id._image);

        }
    }

    public interface ICom {
        int getCount();
        ItemMain getItem(int position) ;
    }

    private void showDetails(int imgAva, int img, String name, String loc) {
        AlertDialog.Builder alertadd = new AlertDialog.Builder(mContext);
        LayoutInflater factory = LayoutInflater.from(mContext);
        final View view = factory.inflate(R.layout.activity_main_fav, null);

//        ImageView imgCall = view.findViewById(R.id.img_dialog_call);
//        ImageView imgNav = view.findViewById(R.id.img_dialog_nav);
//        ImageView imgMain = view.findViewById(R.id.img_main_dialog);
//
//        TextView tvName = view.findViewById(R.id.tv_dialog_name);
//        TextView tvLoc = view.findViewById(R.id.tv_dialog_loc);
//
//        imgMain.setImageResource(img);
//        tvName.setText(name);
//        tvLoc.setText(loc);
//
//        imgCall.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Intent.ACTION_DIAL);
//                intent.setData(Uri.parse("tel:0123456789"));
//                mContext.startActivity(intent);
//            }
//        });
//
//        imgNav.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Intent intent = new Intent(mContext, MapFragment.class);
//                Bundle bundle = new Bundle();
//                String origin = getLocation(latLng);
//                String destination = loc;
//                bundle.putString("origin", origin);
//                bundle.putString("destination", destination);
//                intent.putExtra("Bundel", bundle);
//                mContext.startActivity(intent);
//            }
//        });

        alertadd.setView(view);

        alertadd.show();

    }



}
