package com.example.minhd.drinky.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.minhd.drinky.Fragment.MapFragment;
import com.example.minhd.drinky.Common.Item;
import com.example.minhd.drinky.R;
import com.example.minhd.drinky.Common.TabMessage;
import com.example.minhd.drinky.Adapter.adapterFavorite;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.ArrayList;
import java.util.List;

public class FavoriteActivity extends AppCompatActivity {


    private Spinner spKhuVuc, spSapXep;
    private adapterFavorite adapterr;
    private RecyclerView rclLinea;
    private List<Item> list;
    private View view;
    private ArrayList<Item> listRcl;
    private BottomBar bottomBar;
    private ImageView imgSearch;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStatusBarTranslucent(true);
        setContentView(R.layout.activity_test);

        new Task().execute();
    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    public class Task extends AsyncTask<Void, Void, Void> implements adapterFavorite.ICom {
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }

        @Override
        protected void onPreExecute() {
            bottomBar = findViewById(R.id.bottomBarrrr);
            bottomBar.setDefaultTab(R.id.tab_star);
            bottomBar.setActiveTabColor(Color.parseColor("#d4145a"));
            spKhuVuc = findViewById(R.id.spn_khuvuc);
            spSapXep = findViewById(R.id.spn_sapxep);

            imgSearch = findViewById(R.id.img_searcProfile);
            adapterr = new adapterFavorite(this, getBaseContext());
            rclLinea = findViewById(R.id.rcl_linear);

            imgSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getBaseContext(), SearchActivity.class);
                    startActivity(intent);
                }
            });

            bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
                @Override
                public void onTabSelected(@IdRes int tabId) {
//                messageView.setText(TabMessage.get(tabId, false) +"");
                    if (TabMessage.getPos(tabId, false) == 2) {
                        Intent intent = new Intent(getBaseContext(), MapFragment.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();

                    }
                    if (TabMessage.getPos(tabId, false) == 1) {
                        Intent intent = new Intent(getBaseContext(), MainActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                    }
                    if (TabMessage.getPos(tabId, false) == 3) {

                    }
                    if (TabMessage.getPos(tabId, false) == 4) {

                        Intent intent1 = new Intent(getBaseContext(), ProfileActivity.class);
                        intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent1);
                        finish();
                    }
                }
            });

            bottomBar.setOnTabReselectListener(new OnTabReselectListener() {
                @Override
                public void onTabReSelected(@IdRes int tabId) {
                    Toast.makeText(getBaseContext(), TabMessage.get(tabId, true), Toast.LENGTH_LONG).show();
                }
            });

            List<String> listKV = new ArrayList<>();
            listKV.add("Hà Nội");
            listKV.add("Hồ Chí Minh");
            listKV.add("Đà Nẵng");

            List<String> listSapxep = new ArrayList<>();
            listSapxep.add("Tốt Nhất");
            listSapxep.add("Đẹp Nhất");


            ArrayAdapter<String> adapterSX = new ArrayAdapter(getBaseContext(), R.layout.spinner_item, listSapxep);
            adapterSX.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
            spSapXep.setAdapter(adapterSX);
            spSapXep.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    Toast.makeText(getBaseContext(), spSapXep.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            ArrayAdapter<String> adapter = new ArrayAdapter(getBaseContext(), R.layout.spinner_item, listKV);
            adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
            spKhuVuc.setAdapter(adapter);
            spKhuVuc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    Toast.makeText(getBaseContext(), spKhuVuc.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });


            rclLinea.setLayoutManager(new LinearLayoutManager(getBaseContext()));
            rclLinea.setAdapter(adapterr);
            rclLinea.smoothScrollToPosition(0);
            initData();


            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Void... values) {



            super.onProgressUpdate(values);

        }

        @Override
        protected Void doInBackground(Void... voids) {
            return null;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Item getItem(int position) {
            return list.get(position);
        }

        private void initData() {
            list = new ArrayList<>();
            list.add(new Item("Ding Tea", "112 Trần Duy Hưng", R.drawable.dt2));
            list.add(new Item("Toco Toco", "112 Xuân Thủy", R.drawable.dt1));
            list.add(new Item("Feeling Tea", "20 Hồ Tùng Mậu", R.drawable.feeling));
            list.add(new Item("Ding Tea", "112 Trần Duy Hưng", R.drawable.boba));

        }


    }


}
