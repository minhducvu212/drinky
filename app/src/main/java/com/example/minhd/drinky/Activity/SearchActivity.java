package com.example.minhd.drinky.Activity;


import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.example.minhd.drinky.Adapter.AdapterList;
import com.example.minhd.drinky.Common.Item;
import com.example.minhd.drinky.R;
import com.example.minhd.drinky.Adapter.adapterFavorite;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import io.reactivex.disposables.Disposable;

import static com.example.minhd.drinky.Activity.MainActivity.list;


public class SearchActivity extends AppCompatActivity implements
        adapterFavorite.ICom, AdapterList.Imusic, TextWatcher {

    private RecyclerView listSerch;
    private AdapterList adapter ;
    private adapterFavorite adapterPhim ;
    private Disposable disposableOrigin;
    private final List<Item> filteredList = new ArrayList<>();
    private TextView tvPhim ;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        hideSystemUI();
        setContentView(R.layout.activity_search);
        Toolbar toolbar = findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.left48));
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//               onBackPressed();
//            }
//        });
        findViewByIds() ;


    }

    private void findViewByIds() {
        EditText editText = findViewById(R.id.edt_searchFrag);
        editText.addTextChangedListener(this);
        listSerch = findViewById(R.id.list_searchAct);
        tvPhim = findViewById(R.id.tv_phim);
        tvPhim.setVisibility(View.INVISIBLE);
        adapterPhim = new adapterFavorite(this, getApplicationContext());
        listSerch.setLayoutManager(new LinearLayoutManager(this));
        listSerch.smoothScrollToPosition(0);
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    private void hideSystemUI() {
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true ;
    }

    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hideSystemUI();
    }

    private List<Item> filter(List<Item> mList, String query) {
        query = query.toLowerCase();

        for(Item item : mList){
            if ((convert(item.getName().toLowerCase())).contains(query)) {
                Log.d("TAGGNAME", convert(item.getName().toLowerCase()));

                filteredList.add(item);
                adapter = new AdapterList(this, this, this);
                tvPhim.setVisibility(View.VISIBLE);
                listSerch.setAdapter(adapterPhim);
                adapter.notifyDataSetChanged();
            }

        }
        Log.d("TAGGNAME", "========================");

        return filteredList;
    }

    @RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
    private String convert(String s) {
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("").replaceAll("Đ", "D").replace("đ", "d");
    }

    @Override
    public int getCount() {
        return filteredList.size();
    }

    @Override
    public Item getItem(int position) {
        return filteredList.get(position);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        filteredList.clear();

        if (disposableOrigin != null && !disposableOrigin.isDisposed()) {
            disposableOrigin.dispose();
        }
        String content = s.toString().trim();
        filter(list, content) ;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
