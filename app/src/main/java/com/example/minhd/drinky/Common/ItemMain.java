package com.example.minhd.drinky.Common;

/**
 * Created by minhd on 17/11/29.
 */

public class ItemMain {

    private int img ;

    public ItemMain(int img) {
        this.img = img;
    }

    public int getImg() {
        return img;
    }

    public void setImg(int img) {
        this.img = img;
    }
}
