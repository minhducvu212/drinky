package com.example.minhd.drinky.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.minhd.drinky.Adapter.adapterMain;
import com.example.minhd.drinky.CircleRefreshLayout.CircleRefreshLayout;
import com.example.minhd.drinky.Common.Item;
import com.example.minhd.drinky.Common.ItemMain;
import com.example.minhd.drinky.Common.TabMessage;
import com.example.minhd.drinky.Fragment.MapFragment;
import com.example.minhd.drinky.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks, com.example.minhd.drinky.Adapter.adapterMain.ICom {

    private static final int PERMISSION_ACCESS_COARSE_LOCATION = 101;

    private int[] images = {R.drawable.toco, R.drawable.dingtea, R.drawable.feelingtea, R.drawable.hk,
            R.drawable.royaltea, R.drawable.saytea, R.drawable.boba, R.drawable.uncletea,
            R.drawable.toco, R.drawable.dingtea, R.drawable.feelingtea, R.drawable.hk,
            R.drawable.royaltea, R.drawable.saytea, R.drawable.boba, R.drawable.uncletea};

    private String[] namess = {"Toco Toco", "Ding Tea", "Feeling Tea", "Gongcha", "KFC", "Lotte", "HighLands", "Uncle Tea",
            "Toco Toco", "Ding Tea", "Feeling Tea", "Gongcha", "KFC", "Lotte", "HighLands", "Uncle Tea"};

    private String[] locss = {"59 Hồ Tùng Mậu", "111 trần Duy Hưng", "122 Xuân Thủy", "36 Trần Quốc Hoàn",
            "69 Phạm Văn Đồng", "36 Phạm Hùng", "43 Phạm Văn Đồng", "96 Cấu Giấy",
            "59 Hồ Tùng Mậu", "111 trần Duy Hưng", "122 Xuân Thủy", "36 Trần Quốc Hoàn",
            "69 Phạm Văn Đồng", "36 Phạm Hùng", "43 Phạm Văn Đồng", "96 Cấu Giấy"};

    private int[] imagesShow = {R.drawable.dingteaa, R.drawable.foody, R.drawable.trasua_feeling, R.drawable.dingtea_show,
            R.drawable.dingteaa, R.drawable.foody, R.drawable.trasua_feeling, R.drawable.dingtea_show,
            R.drawable.dingteaa, R.drawable.foody, R.drawable.trasua_feeling, R.drawable.dingtea_show,
            R.drawable.dingteaa, R.drawable.foody, R.drawable.trasua_feeling, R.drawable.dingtea_show};

    private View cell;

    public static List<Item> list;

    private CircleRefreshLayout mRefreshLayout;


    private ImageView imgSearch;

    public static LatLng latLng;

    private GoogleApiClient googleApiClient;

    public static Geocoder geocoderr;

    private adapterMain adapterMain;

    private RecyclerView rclNgon, rclKhuyenMai, rclLa;

    private List<ItemMain> mainList;

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    protected void setStatusBarTranslucent(boolean makeTranslucent) {
        if (makeTranslucent) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setStatusBarTranslucent(true);
        setContentView(R.layout.activity_main);
        googleApiClient = new GoogleApiClient.Builder(this, this, this).addApi(LocationServices.API).build();

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_ACCESS_COARSE_LOCATION);
        }

        list = new ArrayList<>();

        for (int i = 0; i < images.length; i++) {
            list.add(new Item(namess[i], locss[i], images[i]));
        }


        imgSearch = findViewById(R.id.imv_search);

        imgSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), SearchActivity.class);
                startActivity(intent);
            }
        });

        rclNgon = findViewById(R.id.rcl_ngon);
        rclKhuyenMai = findViewById(R.id.rcl_khuyen_mai);
        rclLa = findViewById(R.id.rcl_la);

        adapterMain = new adapterMain(this, this);

        rclNgon.setLayoutManager(new LinearLayoutManager(getBaseContext(), LinearLayoutManager.HORIZONTAL, true));
        rclNgon.setAdapter(adapterMain);
        rclNgon.smoothScrollToPosition(images.length - 1);

        rclKhuyenMai.setLayoutManager(new LinearLayoutManager(getBaseContext(), LinearLayoutManager.HORIZONTAL, true));
        rclKhuyenMai.setAdapter(adapterMain);
        rclKhuyenMai.smoothScrollToPosition(images.length - 1);

        rclLa.setLayoutManager(new LinearLayoutManager(getBaseContext(), LinearLayoutManager.HORIZONTAL, true));
        rclLa.setAdapter(adapterMain);
        rclLa.smoothScrollToPosition(images.length - 1);

        initData();

        mRefreshLayout = findViewById(R.id.refresh_layout);
        mRefreshLayout.setOnRefreshListener(
                new CircleRefreshLayout.OnCircleRefreshListener() {
                    @Override
                    public void refreshing() {
                        refeshItem();

                    }

                    @Override
                    public void completeRefresh() {

                    }

                });


        BottomBar bottomBar = findViewById(R.id.bottomBar);
        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
//                messageView.setText(TabMessage.get(tabId, false) +"");
                if (TabMessage.getPos(tabId, false) == 2) {
                    Intent intent = new Intent(getBaseContext(), MapFragment.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();

                }
                if (TabMessage.getPos(tabId, false) == 1) {

                }
                if (TabMessage.getPos(tabId, false) == 3) {

                    Intent intent1 = new Intent(getBaseContext(), FavoriteActivity.class);
                    intent1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent1);
                    finish();

                }
                if (TabMessage.getPos(tabId, false) == 4) {
                    Intent intent2 = new Intent(getBaseContext(), ProfileActivity.class);
                    intent2.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent2);
                    finish();
                }

            }
        });

        bottomBar.setOnTabReselectListener(new OnTabReselectListener() {
            @Override
            public void onTabReSelected(@IdRes int tabId) {
                Toast.makeText(getApplicationContext(), TabMessage.get(tabId, true), Toast.LENGTH_LONG).show();
            }
        });

    }

    private void initData() {
        mainList = new ArrayList<>() ;
        for (int i = 0; i < images.length; i++) {
            mainList.add(new ItemMain(images[i]));
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_ACCESS_COARSE_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // All good!
                } else {
                    Toast.makeText(this, "Need your location!", Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }


    private void refeshItem() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                mRefreshLayout.finishRefreshing();

            }
        }, 3000);

//        mRefreshLayout.finishRefreshing();

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

            double lat = lastLocation.getLatitude(), lon = lastLocation.getLongitude();
            latLng = new LatLng(lat, lon);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public int getCount() {
        return mainList.size();
    }

    @Override
    public ItemMain getItem(int position) {
        return mainList.get(position);
    }
}